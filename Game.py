
from pygame.locals import *

from PyKostky import FPS
from PyKostky import PPM
from PyKostky import SCREEN_WIDTH
from PyKostky import SCREEN_HEIGHT
from PyKostky import TIME_STEP

from Box2D import b2Vec2,b2World
from Box2D.b2 import *


from Ground import *
from Box import *

from Ball import *
from ContactListener import *

class Game(object):
    def __init__(self, s,initPosBoxes,initPosPad,platforms):
        self.isGameLoop = 0
        self.screen = s
        self.remList = []
        self.objects = []
        self.boxList = []
        self.createBox2DWorld()
        self.setGameObjects(initPosBoxes,initPosPad,platforms)
        self.clock = pygame.time.Clock()
        self.ballsLimit = 10
        self.shouldRefresh=1
    def loop(self):
        self.isGameLoop = 1
        while self.isGameLoop == 1:
            self.screen.fill((25, 95, 161))
            for event in pygame.event.get():
                if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                    self.isGameLoop = 0
                if event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.MOUSEBUTTONUP or event.type == pygame.MOUSEMOTION:
                    self.ball.onMouseEvent(event)
           
            v = self.updateAndCheckWinLooseConditions()
            if(v==3):
                r= self.won()
                if(r!=0):
                    return r
            if(v==2):
                r= self.lose()
                if(r!=0):
                    return r
            if(v==1):
                self.draw()
                
        self.exitGame()

    def exitGame(self):
        self.isGameLoop = 0
        pygame.quit()
        exit()

    def draw(self):
        for obj in self.objects:
            obj.update(TIME_STEP)
            obj.draw()
        self.world.Step(TIME_STEP, 10, 10)
        self.world.ClearForces()

        for obj in self.remList:
            obj.remove()
            self.objects.remove(obj)
            self.remList.remove(obj)
            self.boxList.remove(obj)
        
        pygame.display.flip()
        self.clock.tick(FPS)
    
    def createBox2DWorld(self):
        self.gravity = b2Vec2(0, -10)
        self.world = b2World(gravity = self.gravity, doSleep = True)
        self.world.contactListener = ContactListener()
        self.world.contactListener.setGame(self)
    
    def updateAndCheckWinLooseConditions(self):
        ballsLeft = self.ballsLimit - self.ball.resetCount;
        boxLeft = len( self.boxList);
        
        myFont = pygame.font.Font('freesansbold.ttf', 15)
        ballsLeftTxt = myFont.render('Balls left: '+str(ballsLeft), True, (255, 255, 255))
        boxLeftTxt = myFont.render('Box left: ' +str(boxLeft), True, (255, 255, 255))
        
        self.screen.blit(ballsLeftTxt, ((4), (10)))
        self.screen.blit(boxLeftTxt, ((self.screen.get_width()-90), (10)))
        
        if(ballsLeft<0):
            return 2

        if(boxLeft==0):
            return 3 
        
        return 1;
    def won(self):
        return self.renderEndScreen('You WON', 'Continue',3)
        
    def lose(self):
        return self.renderEndScreen('You Lose', 'Restart',2)
        
    def renderEndScreen (self,txt,btn,r):
        titleFont = pygame.font.Font('freesansbold.ttf', 80)
        btnFont = pygame.font.Font('freesansbold.ttf', 30)
        gm = titleFont.render(txt, True, (255, 255, 255))
        btnC = btnFont.render(btn, True, (255,199,20))
        btnE = btnFont.render("Close", True, (255,199,20))
        btnMM = btnFont.render("Main Menu", True, (255,199,20))
        self.screen.blit(gm,  ((gm.get_rect(centerx=self.screen.get_width()/2).x),150))
        btnnc = self.screen.blit(btnC, ((btnC.get_rect(centerx=self.screen.get_width()/2).x),280))
        btnne = self.screen.blit(btnE, (280,350))
        btnmm = self.screen.blit(btnMM, (400,350))
        for event in pygame.event.get():
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if btnnc.collidepoint(pos):
                    return r
                if btnne.collidepoint(pos):
                    self.exitGame()
                if btnmm.collidepoint(pos):
                    return -1    
        pygame.display.flip()
        return 0
    def remove(self, box):
        self.remList.append(box)

    def setGameObjects(self,initPosBoxes,initPosPad,platforms):
        self.ground = Ground(self.screen ,self.world)
        self.ground.createBody((80, 0.1))
        self.ground.setColor((0, 0, 0))
        self.ground.setUserBody("floor")
        self.objects.append(self.ground)

        self.upGround = Ground(self.screen, self.world)
        self.upGround.createBody((80, 0.1))
        self.upGround.setPosition((0, 48))
        self.ground.setColor((0, 0, 0))
        self.objects.append(self.upGround)
        self.leftGround = Ground(self.screen, self.world)
        self.leftGround.createBody((0.1, 48))
        self.leftGround.setPosition((0, 0))
        self.ground.setColor((0, 0, 0))
        self.objects.append(self.leftGround)
        self.rightGround = Ground(self.screen, self.world)
        self.rightGround.createBody((0.1, 48))
        self.rightGround.setPosition((80, 0))
        self.ground.setColor((0, 0, 0))

        self.objects.append(self.rightGround)
    
        for platformPos in platforms:
            platform = Ground(self.screen, self.world)
            platform.createBody((1, 0.5))
            platform.setPosition(platformPos)
            platform.setColor((0, 255, 0, 255))
            platform.setUserBody = "platform"
            self.objects.append(platform)
        for initBoxPos in initPosBoxes:
            box = Box(self.screen, self.world)
            box.createBody((1, 1))
            box.setPosition(initBoxPos)
            box.setColor((255, 255, 0, 255))
            self.objects.append(box)
            self.boxList.append(box)
        self.ball = Ball(self.screen, self.world,initPosPad)
        self.objects.append(self.ball);
