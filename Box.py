import pygame

import Box2D
from Box2D.b2 import *

from PyKostky import PPM
from PyKostky import SCREEN_HEIGHT

from GameObject import GameObject

class Box(GameObject):
    def __init__(self, s, pWorld):
        super(Box, self).__init__(s, pWorld)
        self.color = (0, 255, 255, 255)
        self.isScored = False # jesli box zapunktowal ustawiamy mu true

    def createBody(self, b):
        self.body = self.world.CreateDynamicBody(position = self.position, angle = 0)
        self.box = self.body.CreatePolygonFixture(box = b, density = 1, friction = .3)
        self.body.linearVelocity = vec2(0, 0)
        self.body.userData = self
        #self.body.active = False
      

    def draw(self):
        for fixture in self.body.fixtures:
            shape = fixture.shape
            vertices=[(self.body.transform*v)*PPM for v in shape.vertices]
            vertices=[(v[0], SCREEN_HEIGHT-v[1]) for v in vertices]
            pygame.draw.polygon(self.screen, self.color, vertices)

    def setPosition(self, pos):
        self.position = pos
        self.body.position = self.position

    def update(self, dt):
        pass       

    def remove(self):
        self.world.DestroyBody(self.body)