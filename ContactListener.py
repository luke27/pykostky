#!/usr/bin/env python
import types

from Box2D import *
from GameObject import GameObject
from Ball import Ball
from Box import Box
DEBUG = False



class ContactListener(b2ContactListener):
    def __init__(self):
        b2ContactListener.__init__(self)
        self.begin = 0
        self.pre = 0
        self.post = 0
        self.end = 0
        self.score = 0
                
            
    def BeginContact(self, contact):
        self.begin += 1
        #print 'Contact Begin' + str(self.begin)
        bodyA = contact.fixtureA.body
        bodyB = contact.fixtureB.body

        #if(bodyA.userData is Box):
        #bodyA.force = b2Vec2(0.0, 10.0)
        #print '/n/n/n/n ' + str(type(bodyA.userData)) + '/n/n/n/n'
        
    def EndContact(self, contact):
        self.end += 1
        #print 'Contact End' + str(self.end)
        
    def PreSolve(self, contact, oldManifold):
        self.pre += 1
        #print 'Contact PreSolve' + str(self.pre)
        
    def PostSolve(self, contact, impulse):
        self.post += 1
        #print 'Contact PostSolve' + str(self.post)
        bodyA = contact.fixtureA.body
        bodyB = contact.fixtureB.body
        #print "BodyA ", bodyA.userData, " BodyB ", bodyB.userData
        if (bodyA.userData == "floor" and isinstance(bodyB.userData, Ball)) or (isinstance(bodyB.userData, Ball) and bodyB.userData == "floor"):
            if isinstance(bodyA.userData, Ball):
                #print '---------A'
                bodyA.userData.isToReset = True
            elif isinstance(bodyB.userData, Ball):
                #print '---------B'
                bodyB.userData.isToReset = True

        if (bodyA.userData == "floor" and isinstance(bodyB.userData, Box)) or (isinstance(bodyA.userData, Box) and bodyB.userData == "floor"):
            if isinstance(bodyA.userData, Box):
                self.addScore(bodyA.userData)
            elif isinstance(bodyB.userData, Box):
                self.addScore(bodyB.userData)

    def addScore(self, box):
        if box.isScored == False:
            self.score += 10
            box.isScored = True
            self.game.remove(box)
        #print 'Score ', self.score

    def getScore(self):
        return self.score
    
    def setGame(self, game):
        self.game = game


