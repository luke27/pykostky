import pygame
from pygame import draw, display, rect
from pygame.locals import *


def showMenu(s):
    titleFont = pygame.font.Font('freesansbold.ttf', 80)
    gm = titleFont.render('Game', True, (255,199,20))
    gmpos =gm.get_rect(centerx=s.get_width()/2);
    ab = titleFont.render('About', True, (255,199,20))
    while True:
        s.fill((25, 95, 161))
        gmb = s.blit(gm, (gmpos.x,100))
        abb = s.blit(ab, (gmpos.x, 300))
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if gmb.collidepoint(pos):
                    return 3
                if abb.collidepoint(pos):
                    return 2
        pygame.display.update()
def showAbout(s):
    titleFont = pygame.font.Font('freesansbold.ttf', 30)
    textFont = pygame.font.Font('freesansbold.ttf', 20)
    t1 = textFont.render('Game created during class', True, (255,199,20))
    t2 = textFont.render('Pracownia jezykow skryptowych', True, (255,199,20))
    t3 = textFont.render('Video Games Production', True, (255,199,20))
    t4 = textFont.render('Jagiellonian University', True, (255,199,20))
    ab = titleFont.render('Back', True, (0,0,0))
    while True:
        s.fill((25, 95, 161))
        s.blit(t1, ((t1.get_rect(centerx=s.get_width()/2).x),50))
        s.blit(t2, ((t2.get_rect(centerx=s.get_width()/2).x),100))
        s.blit(t3, ((t3.get_rect(centerx=s.get_width()/2).x),150))
        s.blit(t4, ((t4.get_rect(centerx=s.get_width()/2).x),200))
        abb = s.blit(ab, (s.get_width()-100, s.get_height()-50))
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if abb.collidepoint(pos):
                    return 1
        pygame.display.flip()

def showBoardsOptions(s,xmlbrds,CURRENT_LEVEL):
    
    font = pygame.font.Font('freesansbold.ttf', 25)
    titleFont = pygame.font.Font('freesansbold.ttf', 30)
    ab = titleFont.render('Back', True, (0,0,0))
    brds=[]
    z=0
    for xmlb in xmlbrds:
        z=z+1
        if(z<=CURRENT_LEVEL):
            tf = font.render(xmlb, True, (255,199,20))
        else:
            tf = font.render(xmlb, True, (61,71,1))
        brds.append(tf)
    while True:
        s.fill((25, 95, 161))
        abb = s.blit(ab, (s.get_width()-100, s.get_height()-50))
        brdsbxs=[]
        i=0
        for brd in brds:
            i=i+1
            tb=s.blit(brd, ((brd.get_rect(centerx=s.get_width()/2).x), (i*80)))
            if(i<=CURRENT_LEVEL):
                brdsbxs.append(tb)
                
        for event in pygame.event.get():
            if event.type == QUIT or (event.type == KEYDOWN and event.key == K_ESCAPE):
                pygame.quit()
                exit()
            if event.type == pygame.MOUSEBUTTONDOWN:
                pos = event.pos
                if abb.collidepoint(pos):
                    return -1
                z=0
                for bx in brdsbxs:
                    if bx.collidepoint(pos):
                        return z
                    z=z+1
        pygame.display.update()
