#!/usr/bin/env python
import types

from Box2D import *
from GameObject import GameObject
from Ball import Ball
from Box import Box

DEBUG = False


class ContactListener(b2ContactListener):
    def __init__(self):
        b2ContactListener.__init__(self)
        self.begin = 0
        self.pre = 0
        self.post = 0
        self.end = 0
        
    def BeginContact(self, contact):
        self.begin += 1
        #print 'Contact Begin' + str(self.begin)
        bodyA = contact.fixtureA.body
        bodyB = contact.fixtureB.body

        #if(bodyA.userData is Box):
        #bodyA.force = b2Vec2(0.0, 10.0)
        print '/n/n/n/n ' + str(type(bodyA.userData)) + '/n/n/n/n'
        
    def EndContact(self, contact):
        self.end += 1
        #print 'Contact End' + str(self.end)
        
    def PreSolve(self, contact, oldManifold):
        self.pre += 1
        #print 'Contact PreSolve' + str(self.pre)
        
    def PostSolve(self, contact, impulse):
        self.post += 1
        #print 'Contact PostSolve' + str(self.post)

