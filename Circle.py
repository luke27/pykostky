import pygame

from GameObject import GameObject

from PyKostky import SCREEN_HEIGHT

class Circle(GameObject):
    def __init__(self, s, pWorld):
        super(Circle, self).__init__(s, pWorld)
        self.radius = 20

    def setPosition(self, pos):
        self.position = pos
    
    def setRadius(self, rad):
        self.radius = rad

    def draw(self):
        pygame.draw.circle(self.screen, self.color, (self.position[0], SCREEN_HEIGHT - self.position[1]), self.radius, 2) 


