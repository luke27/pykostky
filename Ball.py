import pygame

import Box2D
from Box2D.b2 import *


from PyKostky import SCREEN_HEIGHT
from PyKostky import PPM
from math import sin, cos, atan2, sqrt

from GameObject import GameObject

class Ball(GameObject):
    def __init__(self, s, pWorld,initPos):
        super(Ball, self).__init__(s, pWorld)
        self.radius =1
        self.startX=initPos[0]
        self.startY=initPos[1]
        self.position = vec2(self.startX, self.startY)
        self.color = ((255,199,20,100))
        self.body1 = self.world.CreateDynamicBody(position = self.position)
        self.circle = self.body1.CreateCircleFixture(radius = self.radius, density = 1, friction = 1, restitution = 0.1)
        self.body1.userData = self
        self.body1.active = False
        self.isToReset = False
        self.resetCount = 0
        self.isSelected = False
    def draw(self):
        pygame.draw.circle(self.screen, self.color, (int(self.position[0] * PPM), int(SCREEN_HEIGHT - self.position[1] * PPM)), self.radius * PPM)
        pygame.draw.circle(self.screen, self.color, (int(self.startX * PPM), int(SCREEN_HEIGHT - self.startY * PPM)), 100, 2)

    def onMouseEvent(self, event):
        if event.type == pygame.MOUSEMOTION and event.buttons[0] == 1:
            pos = event.pos
            posOrig = event.pos
            pos = (pos[0] / PPM, (SCREEN_HEIGHT - pos[1]) / PPM)
            if pos[0] >= self.position[0] - self.radius and pos[0] <= self.position[0] + self.radius and pos[1] >= self.position[1] - self.radius and pos[1] <= self.position[1] + self.radius:
                self.moveBall(posOrig)
                self.isSelected = True
            else:
                state = 1
        elif event.type == pygame.MOUSEBUTTONUP:
            pos = event.pos
            pos = (pos[0] / PPM, (SCREEN_HEIGHT - pos[1]) / PPM)
            #if pos[0] >= self.position[0] - self.radius and pos[0] <= self.position[0] + self.radius and pos[1] >= self.position[1] - self.radius and pos[1] <= self.position[1] + self.radius:
            #    self.releaseBall()
            if self.isSelected == True:
                self.isSelected = False
                self.releaseBall()

    def releaseBall(self):
        self.body1.active = True
        disX = self.position[0] - self.startX
        disY = self.position[1] - self.startY
        dis = sqrt(disX ** 2 + disY ** 2)
        angle = atan2(disY, disX)
        velX = -dis * cos(angle) / 4 * PPM *2
        velY = -dis * sin(angle) / 4 * PPM *2
        self.body1.linearVelocity = vec2(velX, velY)
    
    def update(self, dt):
        self.position = self.body1.position
        if self.isToReset == True:
            self.reset()

    def reset(self):
        self.body1.linearVelocity = vec2(0, 0)
        self.body1.active = False
        self.body1.position = vec2(self.startX, self.startY)
        self.isToReset = False
        self.resetCount += 1

    def moveBall(self, p):
       x = p[0];
       y = SCREEN_HEIGHT - p[1]
       disX = x  - self.startX * PPM 
       disY = y - self.startY * PPM
       if disX**2 + disY ** 2 > 10000:
           angle = atan2(disY, disX)
           x = self.startX * PPM + 100.0 * cos(angle)
           y = self.startY * PPM + 100.0 * sin(angle)
       self.body1.position = vec2(x / PPM, y / PPM)
