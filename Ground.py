import pygame

import Box2D
from Box2D.b2 import *

from PyKostky import PPM
from PyKostky import SCREEN_HEIGHT

from GameObject import GameObject

class Ground(GameObject):
    def __init__(self,s,pWorld):
        super(Ground, self).__init__(s, pWorld)

    def createBody(self, b):
        self.body = self.world.CreateStaticBody(position = self.position, shapes = polygonShape(box=b))
        self.body.userData = "ground"

    def setUserBody(self, userData):
        self.body.userData = userData

    def draw(self):
        for fixture in self.body.fixtures:
            shape = fixture.shape
            vertices=[(self.body.transform*v)*PPM for v in shape.vertices]
            vertices=[(v[0], SCREEN_HEIGHT-v[1]) for v in vertices]
            pygame.draw.polygon(self.screen, self.color, vertices)

    def setPosition(self, pos):
        self.position = pos
        self.body.position = self.position