import pygame
from pygame import locals
import pygame, sys, random, time,menus,json

SCREEN_WIDTH = 800
SCREEN_HEIGHT = 480

NAME = 'PyKostky'
PPM = 10
FPS = 60
TIME_STEP = 1.0 / FPS


from Game import *

def runGame(s,i):
    json_data = open('data.json')
    data = json.load(json_data)
    xmlbrds=data["boards"]
    d=getBoardData(xmlbrds,i)
    json_data.close()
    game = Game(s,d[0],d[1],d[2])
    game.loop()
    
def getBoardData(dom,selected):
    boxsc=[]
    platforms=[]
    board = dom[selected]
    pad = board["pad"]
    padCords=(int(pad["x"]),int(pad["y"]))
    bxs = board["bxs"]
    for bx in bxs:
        boxsc.append((int(bx["x"]),int(bx["y"])))
    ptfms = board["platforms"]
    for ptfm in ptfms:
        platforms.append((int(ptfm["x"]),int(ptfm["y"])))

    return [boxsc,padCords,platforms]

def main():
    print('PyKostky')
    pygame.init();
    s = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT), 0, 32)
    runGame(s,0)

if __name__ == "__main__":
    main()
