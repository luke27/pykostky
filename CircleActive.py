import pygame
from pygame import *

import Box2D
from Box2D.b2 import *

from PyKostky import PPM
from PyKostky import SCREEN_HEIGHT
from PyKostky import StartX
from PyKostky import StartY

from GameObject import GameObject


from math import sin, cos, atan2, sqrt

class CircleActive(GameObject):
    def __init__(self, s, pWorld):
        super(CircleActive, self).__init__(s, pWorld)
        self.radius = 20
        self.halfSize = self.radius
        self.isThrowed = False

    def draw(self):
        pygame.draw.circle(self.screen, self.color, (int(self.position[0]), int(self.position[1])), self.radius)

    def moveBall(self, p):
        x = p[0];
        y = p[1]
        disX = x - StartX
        disY = y - StartY
        if disX**2 + disY ** 2 > 11000:
            angle = atan2(disY, disX)
            x = StartX + 100.0 * cos(angle)
            y = StartY + 100.0 * sin(angle)
        self.position = vec2(x, y)

    def throwBall(self):
        self.body = self.world.CreateDynamicBody(position = self.position)
        circle = self.body.CreateCircleFixture(radius = self.radius * PPM, density = 1, friction = 3.3, restitution = 0.1)
        disX = self.position[0] - StartX
        disY = self.position[1] - StartY
        dis = sqrt(disX ** 2 + disY ** 2)
        ang = atan2(disY, disX)

        self.body.linearVelocity = vec2(-dis * cos(ang) / 4, -dis * sin(ang) / 4)
        #self.body.force = vec2(-300000, 0)
        self.isThrowed = True

    def update(self, dt):
        if self.isThrowed == True:
            self.position = vec2(self.body.position[0],self.body.position[1])

    def onMouseEvent(self, event):
        state = 0
        if event.type == pygame.MOUSEMOTION and event.buttons[0] == 1:
            pos = event.pos
            if pos[0] >= self.position[0] - self.halfSize and pos[0] <= self.position[0] + self.halfSize and pos[1] >= self.position[1] - self.halfSize and pos[1] <= self.position[1] + self.halfSize:
                print("Click", pos)
                self.moveBall(pos)
                state = 1
        if event.type == pygame.MOUSEBUTTONUP and event.button == 1:
            self.throwBall()
            state = 0

        
