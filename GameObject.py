import Box2D
from Box2D.b2 import *

class GameObject(object):
    def __init__(self, s, pWorld):
        self.screen = s
        self.position = vec2(0, 0)
        self.color = (0, 0, 0, 255)
        self.world = pWorld

    def setPosition(self, pos):
        self.position = pos

    def getPosition(self):
        return self.position

    def setColor(self, col):
        self.color = col

    def draw(self):
        pass

    def createBody(self):
        pass

    def update(self, dt):
        pass

